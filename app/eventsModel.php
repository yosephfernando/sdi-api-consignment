<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class eventsModel extends Model
{
  protected $table = 'GS_PARTICIPATION_BYCTR';
  //protected $primaryKey = '';
  //protected $guarded  = ['id_pk'];
  public $timestamps = false;
}
