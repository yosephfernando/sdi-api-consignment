<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posShowroomMasterModel extends Model
{
  protected $table = 'POS_SHOWROOM_MASTER';
  //protected $primaryKey = 'psr_entity_id';
  protected $guarded  = [];
  public $timestamps = false;
}
