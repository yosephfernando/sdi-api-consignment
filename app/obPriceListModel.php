<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class obPriceListModel extends Model
{
  protected $table = 'OB_PRICE_LIST';
  //protected $primaryKey = '';
  //protected $guarded  = ['id_pk'];
  public $timestamps = false;
}
