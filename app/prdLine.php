<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prdLine extends Model
{
  protected $table = 'IM_PRD_LINE';
  //protected $primaryKey = 'pl_prd_line_code';
  protected $guarded  = [];
  public $timestamps = false;
}
