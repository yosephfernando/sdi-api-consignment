<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class webSalesDetailModel extends Model
{
  protected $table = 'SO_WEB_SALES_DETAIL';
  protected $primaryKey = 'id_pk';
  protected $guarded  = ['id_pk'];
  public $timestamps = false;
}
