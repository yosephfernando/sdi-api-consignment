<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gsGenHardcodedModel extends Model
{
  protected $table = 'GS_GEN_HARDCODED';
  //protected $primaryKey = '';
  //protected $guarded  = ['id_pk'];
  protected $fillable = ['gh_sys', 'gh_function_name', 'gh_sequence_no', 'gh_function_desc', 'gh_min_seq_no', 'gh_max_seq_no', 'gh_last_seq_no', 'gh_group_menu_id', 'gh_parent_menu', 'gh_menu_level', 'gh_menu_flag', 'gh_image', 'gh_password', 'gh_form', 'gh_menu_seq', 'gh_old_app', 'gh_function_code'];
  public $timestamps = false;
}
