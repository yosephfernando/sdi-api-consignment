<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class webSalesHeaderModel extends Model
{
  protected $table = 'SO_WEB_SALES_HEADER';
  protected $primaryKey = 'wsh_seq_no';
  protected $guarded   = ['wsh_seq_no'];
  public $timestamps = false;
}
