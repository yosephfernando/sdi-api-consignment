<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class imCostStdModel extends Model
{
  protected $table = 'IM_COST_STD';
  //protected $primaryKey = '';
  //protected $guarded  = ['id_pk'];
  public $timestamps = false;
}
