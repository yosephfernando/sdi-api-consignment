<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferHeaderModel extends Model
{
    protected $table = 'IM_TRANSFER_HEADER ';
    protected $primary_key = 'trfh_seqno';
    protected $fillable = ['trfh_seqno', 'trfh_date', 'trfh_status', 'trfh_fr1', 'trfh_fr2', 'trfh_to1', 'trfh_to2', 'trfh_create_user', 'trfh_update_user', 'trfh_desc'];
    public $timestamps = false;
}
