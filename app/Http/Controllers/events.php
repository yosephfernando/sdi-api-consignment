<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\eventsModel;
use Illuminate\Support\Facades\DB;

class events extends Controller
{
    public function index($id, $date)
    {
        $gp_cust_code = explode("~", $id);
		$gp_cust_code1 =  $gp_cust_code[0];
		$gp_cust_code2 = $gp_cust_code[1];
				
		$data = eventsModel::where('gp_cust_code1', $gp_cust_code1)
							->where('gp_cust_code2', $gp_cust_code2)
							->where('gp_validfr', '<=', $date)
							->where('gp_validto', '>=', $date)
                            ->get();
							
        if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return $data;
        }
    }
	
	public function eventDistync($id, $date, $gpAcara)
    {
        $gp_cust_code = explode("~", $id);
		$gp_cust_code1 =  $gp_cust_code[0];
		$gp_cust_code2 = $gp_cust_code[1];
				
		$data = eventsModel::where('gp_cust_code1', $gp_cust_code1)
							->where('gp_cust_code2', $gp_cust_code2)
							->where('gp_validfr', '<=', $date)
							->where('gp_validto', '>=', $date)
							->where('gp_acara', $gpAcara)
							->distinct()
                            ->get();
							
        if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return $data;
        }
    }
}
