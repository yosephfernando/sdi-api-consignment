<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\spgModel;
use Illuminate\Support\Facades\DB;

class spgController extends Controller
{
    public function index($sh_code)
    {
        $data =  DB::select(DB::raw("
          select * from SO_SPG_GIRL_MAN WHERE sgm_active_flag ='Y' AND sgm_cust_code1+'~'+sgm_cust_code2='".$sh_code."'
        "));
        $data_count = count($data);
        if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return $data;
        }
    }
}
