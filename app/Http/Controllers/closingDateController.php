<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClosingDateModel;
use Illuminate\Support\Facades\DB;

class closingDateController extends Controller
{
    public function index($moduleName)
    {
        $data = ClosingDateModel::where('module_name', $moduleName)->get();
    		if(!$data){
              return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
            }else{
              return $data;
            }
        }
}
