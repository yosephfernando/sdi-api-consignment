<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\posShowroomMasterModel;
use Illuminate\Support\Facades\DB;

class posShowroomMaster extends Controller
{
    public function index($psr_showroom1_id)
    {
      $data = posShowroomMasterModel::where('psr_showroom1_id', $psr_showroom1_id)->get();
      if(!$data){
        return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
      }else{
        return $data;
      }
    }

    /* public function store(Request $request)
    {
          $data = $request->all();
          $posShowroomMasterModel = new posShowroomMasterModel;
          $saved = $posShowroomMasterModel::insert($data);
          if(!$saved){
            return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
          }else{
            return response('success', 200)->header('Content-Type', 'text/plain');
          }
    }

    public function show($id)
    {
      $data = DB::select( DB::raw("
              SELECT * FROM POS_SHOWROOM_MASTER
              WHERE psr_showroom1_id+'~'+psr_showroom2_id = '".$id."'
          ") );
        if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return $data;
        }
    }

    public function update(Request $request, $id)
    {
          $data = $request->all();

          $psr_entity_id = $data[0]['psr_entity_id'];
          $psr_area = $data[0]['psr_area'];
          $psr_wilayah = $data[0]['psr_wilayah'];
          $psr_rayon = $data[0]['psr_rayon'];
          $psr_branch_id = $data[0]['psr_branch_id'];
          $psr_showr_type = $data[0]['psr_showr_type'];
          $psr_showr_class = $data[0]['psr_showr_class'];
          $psr_showr_catg = $data[0]['psr_showr_catg'];
          $psr_showroom1_id = $data[0]['psr_showroom1_id'];
          $psr_showroom2_id = $data[0]['psr_showroom2_id'];
          $psr_showroom_desc = $data[0]['psr_showroom_desc'];
          $psr_address1 = $data[0]['psr_address1'];
          $psr_address2 = $data[0]['psr_address2'];
          $psr_address3 = $data[0]['psr_address3'];
          $psr_city = $data[0]['psr_city'];
          $psr_zip_code = $data[0]['psr_zip_code'];
          $psr_province = $data[0]['psr_province'];
          $psr_phone = $data[0]['psr_phone'];
          $psr_fax_no = $data[0]['psr_fax_no'];
          $psr_email = $data[0]['psr_email'];
          $psr_npwp = $data[0]['psr_npwp'];
          $psr_contact_person = $data[0]['psr_contact_person'];
          $psr_manager = $data[0]['psr_manager'];
          $psr_whloc_id1 = $data[0]['psr_whloc_id1'];
          $psr_whloc_id2 = $data[0]['psr_whloc_id2'];
          $psr_reg_date = $data[0]['psr_reg_date'];
          $psr_showroom_status = $data[0]['psr_showroom_status'];
          $psr_message1 = $data[0]['psr_message1'];
          $psr_message2 = $data[0]['psr_message2'];
          $psr_update_date = $data[0]['psr_update_date'];
          $psr_user = $data[0]['psr_user'];
          $psr_line_code = $data[0]['psr_line_code'];
          $psr_seq = $data[0]['psr_seq'];
          $psr_group = $data[0]['psr_group'];
          $psr_type = $data[0]['psr_type'];
          $psr_supervisor = $data[0]['psr_supervisor'];
          $psr_korsup = $data[0]['psr_korsup'];
          $psr_korwil = $data[0]['psr_korwil'];
          $psr_message3 = $data[0]['psr_message3'];
          $psr_message4 = $data[0]['psr_message4'];
          $psr_bonus1 = $data[0]['psr_bonus1'];
          $psr_bonus2 = $data[0]['psr_bonus2'];
          $psr_bonus3 = $data[0]['psr_bonus3'];
          if($data != null ){
            $saved =  DB::raw("
                 UPDATE POS_SHOWROOM_MASTER
                 SET psr_entity_id = '".$psr_entity_id."', psr_area = '".$psr_area."', psr_wilayah = '".$psr_wilayah."',
                 psr_rayon = '".$psr_rayon."', psr_branch_id = '".$psr_branch_id."', psr_showr_type = '".$psr_showr_type."',
                 psr_showr_class = '".$psr_showr_class."', psr_showr_catg = '".$psr_showr_catg."', psr_showroom1_id = '".$psr_showroom1_id."',
                 psr_showroom2_id = '".$psr_showroom2_id."', psr_showroom_desc = '".$psr_showroom_desc."', psr_address1 = '".$psr_address1."',
                 psr_address2 = '".$psr_address2."', psr_address3 = '".$psr_address3."', psr_city = '".$psr_city."',
                 psr_zip_code = '".$psr_zip_code."', psr_province = $psr_province,  psr_phone = '".$psr_phone."', psr_fax_no = '".$psr_fax_no."',
                 psr_email = '".$psr_email."', psr_npwp = '".$psr_npwp."', psr_contact_person = '".$psr_contact_person."',
                 psr_manager = '".$psr_manager."', psr_whloc_id1 = '".$psr_whloc_id1."', psr_whloc_id2 = '".$psr_whloc_id2."',
                 psr_reg_date = '".$psr_reg_date."', psr_showroom_status = '".$psr_showroom_status."', psr_message1 = '".$psr_message1."',
                 psr_message2 = '".$psr_message2."', psr_update_date = '".$psr_update_date."', psr_user = '".$psr_user."',
                 psr_line_code = '".$psr_line_code."', psr_seq = '".$psr_seq."', psr_group = '".$psr_group."', psr_type = '".$psr_type."',
                 psr_supervisor = '".$psr_supervisor."', psr_korsup = '".$psr_korsup."', psr_korwil = '".$psr_korwil."',
                 psr_message3 = '".$psr_message3."', psr_message4 = '".$psr_message4."', psr_bonus1 = '".$psr_bonus1."',
                 psr_bonus2 = '".$psr_bonus2."', psr_bonus3 = '".$psr_bonus3."'
                 WHERE psr_showroom1_id+'~'+psr_showroom2_id = '".$id."';
             ");
            if(!$saved){
              return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
            }else{
              return response('success', 200)->header('Content-Type', 'text/plain');
            }
          }else{
            return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
          }
    }

    public function destroy($id)
    {
        $deleted =DB::raw("
            DELETE FROM POS_SHOWROOM_MASTER WHERE psr_showroom1_id+'~'+psr_showroom2_id = '".$id."'
        ");

          if(!$deleted){
            return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
          }else{
            return response('success', 200)->header('Content-Type', 'text/plain');
          }
    } */
}
