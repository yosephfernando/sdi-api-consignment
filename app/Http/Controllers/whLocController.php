<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WhLocModel;
use Illuminate\Support\Facades\DB;

class whLocController extends Controller
{
    public function index($whLoc1, $whLoc2)
    {
        $data = WhLocModel::where('wh_loc_id1', $whLoc1)
                            ->where('wh_loc_id2', $whLoc2)
                            ->get();
		    if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return $data;
        }
    }
}
