<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\webSalesDetailModel;
use Illuminate\Support\Facades\DB;

class webSalesDetail extends Controller
{
    /* public function index()
    {
        $data = webSalesDetailModel::all();
        $data_count = count($data);
        if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          if($data_count > 1){
             return $data;
          }else{
            return response()->json([
                $data
            ]);
          }
        }
    } */

    public function insertSalesDetail(Request $request)
    {
      $data = $request->all();
	  $cData = count($data) - 1;
	  $keyData = null;
	  $valueString = null;
	  
	  for($i=0;$i<=$cData;$i++) {
		   $keys = array_keys($data);
		   if($i == $cData){
			   $keyData .= '['.$keys[$i].']';
		   }else{
			   $keyData .= '['.$keys[$i].']'.", ";
		   }
		   
		   $value = array_values($data);
		   if($i == $cData){
				$valueString .= $value[$i];
		   }else{
			    $valueString .= $value[$i].",";
		   }
	  }
	  	  
	  $saved = DB::insert('INSERT INTO SO_WEB_SALES_DETAIL ('.$keyData.') values('.$valueString.')');
		/* $data = $request->all();
        $webSalesDetailModel = new webSalesDetailModel;
        $saved = $webSalesDetailModel::insert($data); */
        if(!$saved){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return response('success', 200)->header('Content-Type', 'text/plain');
        }
    }

    /* public function show($id)
    {
       $data = webSalesDetailModel::find($id);
       if(!$data){
         return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
       }else{
         return response()->json([
             $data
         ]);
       }
    }

    public function getByFilter(Request $request){
      $datas = $request->all();
      $master_code = $datas[0]['master_code'];
      $from_date = $datas[0]['from_date'];
      $to_date = $datas[0]['to_date'];
      if($datas != null){
              $data = webSalesDetailModel::where('wsd_prd_master_code', 'LIKE', '%'.$master_code.'%')
                                    ->whereBetween('wsd_entry_date', array($from_date, $to_date))->get();

              $data_count = count($data);
              if(!$data){
                return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
              }else{
                 if($data_count != 0){
                   return $data;
                 }else{
                   return response('data not found', 404)->header('Content-Type', 'text/plain');
                 }
              }
      }
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        if($data != null ){
          $saved = webSalesDetailModel::where('id_pk', $id)->update($data[0]);
            if(!$saved){
              return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
            }else{
              return response('success', 200)->header('Content-Type', 'text/plain');
            }
        }else{
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

    public function destroy($id)
    {
        $deleted = webSalesDetailModel::where('id_pk', $id)->delete();
        if(!$deleted){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return response('success', 200)->header('Content-Type', 'text/plain');
        }
    } */
}
