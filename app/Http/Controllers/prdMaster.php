<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\prdMasterModel;
use Illuminate\Support\Facades\DB;

class prdMaster extends Controller
{
    public function index($id)
    {
       $data = prdMasterModel::where('prm_prd_master_code', $id)
							   ->orWhere('prm_barcode', $id)
							   ->get();
		
      if(!$data){
        return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
      }else{
        return $data;
      }
	  
    }
	
	public function getByartAndSize($id, $size)
    {
      $data = prdMasterModel::where('prm_prd_master_code', $id)
							  ->where('prm_prd_size', $size)
							  ->get();
		
      if(!$data){
        return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
      }else{
        return $data;
      }
	  
    }
	
	public function getSugest($keyword)
    {
	  $keyword = strtoupper($keyword);
      $data = prdMasterModel::select(DB::raw('prm_prd_master_code+prm_prd_size+prm_grade AS data'))
							  ->where(DB::raw('UPPER(prm_prd_master_code+prm_prd_size+prm_grade)'), "LIKE","%".$keyword."%")
							  ->get();
		
      if(!$data){
        return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
      }else{
        return $data;
      }
	  
    }
	
	public function getByartc($id, $dates, $step)
    {
		$data = null;
		$subQuery = "(select a.prm_prd_master_code as prd_master_code,a.prm_grade as prd_grade,a.prm_prd_size as prd_size, isnull(b.prd_status,a.prm_target) as prd_status, isnull(b.retail_price,isnull(c.opl_het,0)) as retail_price, isnull(b.cogs_price,isnull(d.cs_standard_unit_cost,0)) as cogs_price,isnull(b.material_cost,0) as material_cost, isnull(b.prod_cost,0) as prod_cost,isnull(b.royalty_cost,0) as royalty_cost from IM_PRD_MASTER a left join (select x.prd_master_code,x.prd_grade,x.prd_size,x.prd_status,x.retail_price,x.cogs_price, x.material_cost,x.prod_cost,x.royalty_cost from IM_PRD_PRICE x inner join (select prd_master_code,prd_grade,prd_size,MAX(startdate) as sdate from IM_PRD_PRICE where startdate<='".$dates."' group by prd_master_code,prd_grade,prd_size) y on y.prd_master_code=x.prd_master_code and y.prd_grade=x.prd_grade and y.prd_size=x.prd_size and y.sdate=x.startdate) b on b.prd_master_code=a.prm_prd_master_code and b.prd_grade=a.prm_grade and b.prd_size=a.prm_prd_size left join OB_PRICE_LIST c on c.opl_price_code='STD' and c.opl_product_id=a.prm_prd_master_code and c.opl_grade=a.prm_grade and c.opl_size=a.prm_prd_size left join IM_COST_STD d on d.cs_prd_master_code=a.prm_prd_master_code and d.cs_grade=a.prm_grade and d.cs_size=a.prm_prd_size)";
		
		if($step == 2){
			$data =  DB::select(DB::raw("
			   select prm_barcode, prm_prd_master_code, prm_grade, prm_prd_size, isnull(b.retail_price,0) as rpisnull, cast(isnull(b.retail_price,0) as numeric(18,0)) as retail_price from IM_PRD_MASTER a left join ".$subQuery." b on b.prd_master_code=a.prm_prd_master_code and b.prd_grade=a.prm_grade and b.prd_size=a.prm_prd_size WHERE (prm_prd_master_code= '".$id."') or prm_barcode= '".$id."'
			"));
		}else if($step == 3){
			$data =  DB::select(DB::raw("
			   select prm_barcode, prm_prd_master_code, prm_grade, prm_prd_size, isnull(b.retail_price,0) as rpisnull, cast(isnull(b.retail_price,0) as numeric(18,0)) as retail_price from IM_PRD_MASTER a left join ".$subQuery." b on b.prd_master_code=a.prm_prd_master_code and b.prd_grade=a.prm_grade and b.prd_size=a.prm_prd_size WHERE (prm_prd_master_code+prm_prd_size+prm_grade= '".$id."') or prm_barcode= '".$id."';
			"));
		}	
		
      if(!$data){
        return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
      }else{
        return $data;
      }
	  
    }
	
	/* public function index()
    {
        $data = prdMasterModel::all();
        $data_count = count($data);
        if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          if($data_count > 1){
             return $data;
          }else{
            return response()->json([
                $data
            ]);
          }
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $prdMasterModel = new prdMasterModel;
        $saved = $prdMasterModel::insert($data);
        if(!$saved){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return response('success', 200)->header('Content-Type', 'text/plain');
        }
    }

   

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $prm_prd_line_code = $data[0]['prm_prd_line_code'];
        $prm_prd_group_code = $data[0]['prm_prd_group_code'];
        $prm_prd_sgroup_code = $data[0]['prm_prd_sgroup_code'];
        $prm_prd_model_code = $data[0]['prm_prd_model_code'];
        $prm_raw_mat_used_code = $data[0]['prm_raw_mat_used_code'];
        $prm_washing_collor_code = $data[0]['prm_washing_collor_code'];
        $prm_prd_master_code = $data[0]['prm_prd_master_code'];
        $prm_grade = $data[0]['prm_grade'];
        $prm_prd_size = $data[0]['prm_prd_size'];
        $prm_barcode = $data[0]['prm_barcode'];
        $prm_prd_desc = $data[0]['prm_prd_desc'];
        $prm_prd_short = $data[0]['prm_prd_short'];
        $prm_size_group_code = $data[0]['prm_size_group_code'];
        $prm_mfg_code = $data[0]['prm_mfg_code'];
        $prm_item_group = $data[0]['prm_item_group'];
        $prm_prd_type = $data[0]['prm_prd_type'];
        $prm_no_of_item = $data[0]['prm_no_of_item'];
        $prm_tax_code = $data[0]['prm_tax_code'];
        $prm_default_disc_code = $data[0]['prm_default_disc_code'];
        $prm_um = $data[0]['prm_um'];
        $prm_um_sales = $data[0]['prm_um_sales'];
        $prm_um_purc = $data[0]['prm_um_purc'];
        $prm_conversion_sales = $data[0]['prm_conversion_sales'];
        $prm_conversion_purc = $data[0]['prm_conversion_purc'];
        $prm_unit_weight_gr = $data[0]['prm_unit_weight_gr'];
        $prm_unit_volume_cm3 = $data[0]['prm_unit_volume_cm3'];
        $prm_unit_volume_lt = $data[0]['prm_unit_volume_lt'];
        $prm_wh_control_flag = $data[0]['prm_wh_control_flag'];
        $prm_tech_constrain_flag = $data[0]['prm_tech_constrain_flag'];
        $prm_non_stock_flag = $data[0]['prm_non_stock_flag'];
        $prm_active_flag = $data[0]['prm_active_flag'];
        $prm_creation_date = $data[0]['prm_creation_date'];
        $prm_update_date = $data[0]['prm_update_date'];
        $prm_user = $data[0]['prm_user'];
        $prm_sex = $data[0]['prm_sex'];
        $prm_trend = $data[0]['prm_trend'];
        $prm_target = $data[0]['prm_target'];
        $prm_season = $data[0]['prm_season'];
        $prm_decimal_point = $data[0]['prm_decimal_point'];
        $prm_period = $data[0]['prm_period'];
        $prm_supplier = $data[0]['prm_supplier'];
        $prm_ext1 = $data[0]['prm_ext1'];
        $prm_ext2 = $data[0]['prm_ext2'];
        $Xprm_prd_master_code = $data[0]['Xprm_prd_master_code'];

        if($data != null ){
          $saved =  DB::raw("
               UPDATE IM_PRD_MASTER
               SET prm_prd_line_code = '".$prm_prd_line_code."', prm_prd_group_code = '".$prm_prd_group_code."', prm_prd_sgroup_code = '".$prm_prd_sgroup_code."',
               prm_prd_model_code ='".$prm_prd_model_code."', prm_raw_mat_used_code='".$prm_raw_mat_used_code."', prm_washing_collor_code='".$prm_washing_collor_code."', prm_prd_master_code='".$prm_prd_master_code."', prm_grade='".$prm_grade."',
               prm_prd_size='".$prm_prd_size."', prm_barcode='".$prm_barcode."', prm_prd_desc='".$prm_prd_desc."', prm_prd_short='".$prm_prd_short."', prm_size_group_code='".$prm_size_group_code."', prm_mfg_code='".$prm_mfg_code."', prm_item_group='".$prm_item_group."'
               prm_prd_type='".$prm_prd_type."', prm_no_of_item='".$prm_no_of_item."', prm_tax_code='".$prm_tax_code."', prm_default_disc_code='".$prm_default_disc_code."', prm_um='".$prm_um."', prm_um_sales='".$prm_um_sales."', prm_um_purc='".$prm_um_purc."'
               prm_conversion_sales='".$prm_conversion_sales."', prm_conversion_purc='".$prm_conversion_purc."', prm_unit_weight_gr='".$prm_unit_weight_gr."', prm_unit_volume_cm3='".$prm_unit_volume_cm3."', prm_unit_volume_lt='".$prm_unit_volume_lt."', prm_wh_control_flag='".$prm_wh_control_flag."'
               prm_tech_constrain_flag='".$prm_tech_constrain_flag."', prm_non_stock_flag='".$prm_non_stock_flag."', prm_active_flag='".$prm_active_flag."', prm_creation_date='".$prm_creation_date."', prm_update_date='".$prm_update_date."', prm_user='".$prm_user."', prm_sex='".$prm_sex."'
               prm_trend='".$prm_trend."', prm_target='".$prm_target."', prm_season='".$prm_season."', prm_decimal_point='".$prm_decimal_point."', prm_period='".$prm_period."', prm_supplier='".$prm_supplier."', prm_ext1='".$prm_ext1."', prm_ext2='".$prm_ext2."', Xprm_prd_master_code='".$Xprm_prd_master_code."';
           ");
            if(!$saved){
              return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
            }else{
              return response('success', 200)->header('Content-Type', 'text/plain');
            }
        }else{
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }
    } */

    /* public function getByFilter(Request $request){
      $datas = $request->all();
      $master_code = $datas[0]['prm_prd_master_code'];
      $from_date = $datas[0]['prm_creation_date_from'];
      $to_date = $datas[0]['prm_creation_date_to'];
      if($datas != null){
              $data = prdMasterModel::where('prm_prd_master_code', 'LIKE', '%'.$master_code.'%')
                                    ->whereBetween('prm_creation_date', array($from_date, $to_date))
                                    ->paginate(100);

              $data_count = count($data);
              if(!$data){
                return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
              }else{
                if($data_count > 1){
                   return $data;
                }else if($data_count == 1){
                  return response()->json([
                      $data
                  ]);
                }else{
                  return "Failed";
                }
              }
      }
    } */

    /* public function destroy($id)
    {
      $deleted =DB::raw("
          DELETE FROM IM_PRD_MASTER WHERE prm_prd_master_code+'~'+prm_grade+'~'+prm_prd_size = '".$id."'
      ");

        if(!$deleted){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return response('success', 200)->header('Content-Type', 'text/plain');
        }
    } */
}
