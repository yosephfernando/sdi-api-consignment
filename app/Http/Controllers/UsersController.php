<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public function index ($email){
        $data = User::where('email', $email)->first();
        if($data != null){
          return $data;
        }else{
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }
    }
}
