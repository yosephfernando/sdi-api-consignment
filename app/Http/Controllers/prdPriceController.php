<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\prdMasterModel;
use Illuminate\Support\Facades\DB;

class prdPriceController extends Controller
{
    public function index()
    {
        $data = prdPriceModel::all();
        $data_count = count($data);
        if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return $data;
        }
    }

    public function getByFilter(Request $request){
      $datas = $request->all();
      $master_code = $datas[0]['prm_prd_master_code'];
      $from_date = $datas[0]['prm_creation_date_from'];
      $to_date = $datas[0]['prm_creation_date_to'];
      if($datas != null){
              $data = prdMasterModel::selectRaw("IM_PRD_PRICE.*")
              ->join('IM_PRD_PRICE', function($join){
                  $join->on('IM_PRD_MASTER.prm_prd_master_code', '=', 'IM_PRD_PRICE.prd_master_code');
                  $join->on('IM_PRD_MASTER.prm_prd_size', '=', 'IM_PRD_PRICE.prd_size');
                  $join->on('IM_PRD_MASTER.prm_grade', '=', 'IM_PRD_PRICE.prd_grade');
              })
              ->where('IM_PRD_MASTER.prm_prd_master_code', 'LIKE', '%'.$master_code.'%')
              ->whereBetween('IM_PRD_MASTER.prm_creation_date', array($from_date, $to_date))
              ->paginate(100);

              $data_count = count($data);
              if(!$data){
                return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
              }else{
                if($data_count > 1){
                   return $data;
                }else if($data_count == 1){
                  return response()->json([
                      $data
                  ]);
                }else{
                  return "Failed";
                }
              }
      }
    }
}
