<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class ConsignmentController extends Controller
{
    public function index()
    {
        $data = DB::select( DB::raw("
            select a.wsh_cust_code1+'~'+a.wsh_cust_code2 as wsh_cust_code, a.wsh_so_date, a.wsh_participation_code, b.wsd_so_seq_no,
            b.wsd_prd_master_code, b.wsd_grade, b.wsd_prd_size,
            b.wsd_het_unit_price, b.wsd_qty_sales , a.wsh_disc_acara, a.wsh_disc_acara2,
            ((100 - a.wsh_disc_acara) / 100 * b.wsd_het_unit_price * b.wsd_qty_sales)-
            a.wsh_disc_acara2 / 100 * (((100 - a.wsh_disc_acara) / 100 * b.wsd_het_unit_price * b.wsd_qty_sales)) as total
            from SO_WEB_SALES_HEADER a
            inner join SO_WEB_SALES_DETAIL b on b.wsd_so_seq_no=a.wsh_seq_no
        ") );
        if($data != null){
          return $data;
        }else{
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

    public function showDataById($sh_code)
    {
        $data = DB::select( DB::raw("
            select a.wsh_cust_code1+'~'+a.wsh_cust_code2 as wsh_cust_code, a.wsh_so_date, a.wsh_participation_code, b.wsd_so_seq_no,
            b.wsd_prd_master_code, b.wsd_grade, b.wsd_prd_size,
            b.wsd_het_unit_price, b.wsd_qty_sales , a.wsh_disc_acara, a.wsh_disc_acara2,
            ((100 - a.wsh_disc_acara) / 100 * b.wsd_het_unit_price * b.wsd_qty_sales)-
            a.wsh_disc_acara2 / 100 * (((100 - a.wsh_disc_acara) / 100 * b.wsd_het_unit_price * b.wsd_qty_sales)) as total
            from SO_WEB_SALES_HEADER a
            inner join SO_WEB_SALES_DETAIL b on b.wsd_so_seq_no=a.wsh_seq_no
            where a.wsh_cust_code1+'~'+wsh_cust_code2 = '".$sh_code."'
        ") );
        if($data != null){
          return $data;
        }else{
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }
    }

    public function getByFilter(Request $request)
    {
        $datas = $request->all();
        $data = DB::select( DB::raw("
            select a.wsh_cust_code1+'~'+a.wsh_cust_code2 as wsh_cust_code, a.wsh_so_date, a.wsh_participation_code, b.wsd_so_seq_no,
            b.wsd_prd_master_code, b.wsd_grade, b.wsd_prd_size,
            b.wsd_het_unit_price, b.wsd_qty_sales , a.wsh_disc_acara, a.wsh_disc_acara2,
            ((100 - a.wsh_disc_acara) / 100 * b.wsd_het_unit_price * b.wsd_qty_sales)-
            a.wsh_disc_acara2 / 100 * (((100 - a.wsh_disc_acara) / 100 * b.wsd_het_unit_price * b.wsd_qty_sales)) as total
            from SO_WEB_SALES_HEADER a
            inner join SO_WEB_SALES_DETAIL b on b.wsd_so_seq_no=a.wsh_seq_no
            where b.wsd_prd_master_code = '".$datas[0]['wsd_prd_master_code']."'
            and wsh_so_date between '".$datas[0]['from_date']."' and '".$datas[0]['to_date']."'
        ") );
        if($data != null){
          return $data;
        }else{
          return response('data not found', 404)->header('Content-Type', 'text/plain');
        }
    }
	
	public function listConsignment($sh_code, $date1)
    {		
        /* $data = DB::select( DB::raw("
            select a.wsh_approval, c.gp_desc, a.wsh_so_date, a.wsh_participation_code, b.wsd_so_seq_no, b.wsd_prd_master_code, b.wsd_grade, b.wsd_prd_size,  b.wsd_het_unit_price, b.wsd_qty_sales , a.wsh_disc_acara, a.wsh_disc_acara2, 
			
			b.wsd_het_unit_price * b.wsd_qty_sales - ((100 - a.wsh_disc_acara) / 100 * b.wsd_het_unit_price * b.wsd_qty_sales)- a.wsh_disc_acara2 / 100 * (((100 - a.wsh_disc_acara) / 100 * b.wsd_het_unit_price * b.wsd_qty_sales)) as total, 
			
			(b.wsd_het_unit_price * b.wsd_qty_sales) as gross from SO_WEB_SALES_HEADER a inner join SO_WEB_SALES_DETAIL b on b.wsd_so_seq_no=a.wsh_seq_no 
			inner join GS_PARTICIPATION_BYCTR c on (c.gp_cust_code1+'~'+c.gp_cust_code2) = (a.wsh_cust_code1+'~'+a.wsh_cust_code2) and a.wsh_participation_code = c.gp_acara
			where a.wsh_cust_code1+'~'+a.wsh_cust_code2 = '".$sh_code."' and convert(varchar(10), a.wsh_so_date, 120) = '".$date1."'
        ")); */
		$data = DB::select(DB::raw("select * FROM vpv_penjualan_konsinyasi where Wh1+'~'+Wh2 = '".$sh_code."' and convert(varchar(10), Tanggal, 120) = '".$date1."'"));
		
        if($data != null){
          return $data;
        }else{
          return response('data not found', 404)->header('Content-Type', 'text/plain');
        }
    }
}
