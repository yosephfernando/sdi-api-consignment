<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\prdLine;
use Illuminate\Support\Facades\DB;

class prdLineController extends Controller
{
    public function index($userId)
    {
		$userId = explode("~", $userId);
		$psr_showroom1_id = $userId[0];
		$psr_showroom2_id = $userId[1];
		
        $data =  DB::select(DB::raw("
           SELECT IM.pl_prd_line_desc, PS.psr_line_code, PS.psr_showroom1_id, PS.psr_showroom2_id FROM POS_SHOWROOM_MASTER AS PS INNER JOIN IM_PRD_LINE AS IM ON PS.psr_line_code = IM.pl_prd_line_code WHERE PS.psr_showroom1_id = '".$psr_showroom1_id."' AND PS.psr_showroom2_id = '".$psr_showroom2_id."'
        "));

        if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return $data;
        }
    }
}
