<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransferHeaderModel;
use Illuminate\Support\Facades\DB;

class approvalController extends Controller
{
    public function updateStatusTrfh(Request $request)
    {
        $params = $request->all();
        $seqno = $params[0];
        $trfh_status = $params[1];
        $trfh_apv_date = $params[2];
        $trfh_user_apv = $params[3];

        $data = TransferHeaderModel::where('trfh_seqno', $seqno)
                                     ->update(['trfh_status' => $trfh_status, 'trfh_apv_date'=>$trfh_apv_date, 'trfh_user_apv' => $trfh_user_apv]);
    		if(!$data){
            return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
            return response('success', 200)->header('Content-Type', 'text/plain');
        }
    }

    public function approval($userId){
      $data =  DB::select(DB::raw("
			   select ghc_parent_menu, ghc_function_name, ghc_function_desc, ghc_group_menu_id FROM GS_USER_MENU_BO, GS_HARD_CODED, GS_GROUP_MENU Where gum_menu_id = ghc_function_name and ggm_group_menu_id = ghc_group_menu_id and gum_user_id = ".$userId." and ghc_sys = 'M' and ghc_form='IMTransferApproval'
			"));
    }
}
