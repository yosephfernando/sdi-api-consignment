<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\webSalesHeaderModel;
use Illuminate\Support\Facades\DB;

class webSalesHeader extends Controller
{
  public function index()
  {
      $data = webSalesHeaderModel::all();
      $data_count = count($data);
      if(!$data){
        return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
      }else{
        if($data_count > 1){
           return $data;
        }else{
          return response()->json([
              $data
          ]);
        }
      }
  }

  public function insertSalesHeader(Request $request)
  {
      $data = $request->all();
	  $cData = count($data);
	  $keyData = null;
	  $valueString = null;
	   for($i=0;$i<$cData;$i++) {
		   $keys = array_keys($data);
		    if($i == 29){
			   $keyData .= '['.$keys[$i].']';
		   }else{
			   $keyData .= '['.$keys[$i].']'.", ";
		   }
		   
		   $value = array_values($data);
		   if($i == 29){
				$valueString .= $value[$i];
		   }else if($keys[$i] == 'wsh_so_date'){
				$date = explode(" ", $value[$i]);
				$date = str_replace("'", "", $date[0]);
				
				$valueString .= "'".date("Y-m-d", strtotime($date))." 00:00:00'".", ";
		   }else{
			    $valueString .= $value[$i].", ";
		   }
	  } 
      /*$webSalesHeaderModel = new webSalesHeaderModel;
      $saved = $webSalesHeaderModel::insert($data); */
	  $saved = DB::insert('INSERT INTO SO_WEB_SALES_HEADER ('.$keyData.') values('.$valueString.')');
	  		

      if(!$saved){
        return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
      }else{
        return response('success', 200)->header('Content-Type', 'text/plain');
      }
  }
  
  public function deleteCons(Request $request)
  {
	 $param = $request->all();
	 $seqNo = $param["wsd_so_seq_no"];
	 $product = $param["product"];
	 
	 $cekApproval = webSalesHeaderModel::join('SO_WEB_SALES_DETAIL', 'SO_WEB_SALES_HEADER.wsh_seq_no', '=', 'SO_WEB_SALES_DETAIL.wsd_so_seq_no')
	 ->where('wsh_seq_no', $seqNo)
	 ->where('wsh_approval', '<>', 'Y');
	 
	 if($cekApproval){
		 DB::statement('EXEC p_DELETE_SO_WEB_SALES_DETAIL '."'".$seqNo."'".', '."'".$product."'");
		 return response('success', 200)->header('Content-Type', 'text/plain');
	 }else{
		 return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
	 }
  }

  /* public function show($id)
  {
     $data = webSalesHeaderModel::find($id);
     if(!$data){
       return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
     }else{
       return response()->json([
           $data
       ]);
     }
  }

  public function getByFilter(Request $request){
    $datas = $request->all();
    $from_date = $datas[0]['from_date'];
    $to_date = $datas[0]['to_date'];
    if($datas != null){
            $data = webSalesHeaderModel::whereBetween('wsh_entry_date', array($from_date, $to_date))->get();
            $data_count = count($data);
            if(!$data){
              return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
            }else{
              if($data_count != 0){
                return $data;
              }else{
                return response('data not found', 404)->header('Content-Type', 'text/plain');
              }
            }
    }
  }

  public function update(Request $request, $id)
  {
      $data = $request->all();
      if($data != null ){
        $saved = webSalesHeaderModel::where('wsh_seq_no', $id)->update($data[0]);
          if(!$saved){
            return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
          }else{
            return response('success', 200)->header('Content-Type', 'text/plain');
          }
      }else{
        return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
      }
  }

  public function destroy($id)
  {
      $deleted = webSalesHeaderModel::where('wsh_seq_no', $id)->delete();
      if(!$deleted){
        return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
      }else{
        return response('success', 200)->header('Content-Type', 'text/plain');
      }
  } */
}
