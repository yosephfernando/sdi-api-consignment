<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\gsGenHardcodedModel;
use Illuminate\Support\Facades\DB;

class gsGenHarcodedController extends Controller
{	
	public function getGsGenByFcFn($fc, $fn, $showroom)
    {
		$tokoCode = substr($showroom, -4);
		$fc = $tokoCode.$fc;
		$fn = $tokoCode.$fn;
        $data = gsGenHardcodedModel::where('gh_sys', 'S')
									 ->where('gh_function_name', $fn)
									 ->where('gh_function_code', $fc)	
									 ->get();
		$dataCount = count($data);
		$notrans = "";
		if($dataCount > 0){
			foreach($data as $datas){
	
				if($datas->gh_last_seq_no <= $datas->gh_max_seq_no){
					$nomax = $datas->gh_max_seq_no;
					$lnmax = strlen($nomax);
					$Seqno = $datas->gh_last_seq_no + 1;
					$lnseq = strlen($Seqno);
					$digit = substr("000000000000",0,$lnmax-$lnseq);
					
					$datasUpdates = array(
						"gh_last_seq_no" => $Seqno
					);
					$dataUpdate = gsGenHardcodedModel::where('gh_sys', 'S')
													   ->where('gh_function_name', $fn)
													   ->where('gh_function_code', $fc)
													   ->update($datasUpdates);
					$notrans = "W".$fn.$digit.$Seqno;
				}
			}
		}else{
					$dataInsertes = array(
						"gh_sys" => "S",
						"gh_function_name" => $fn,
						"gh_sequence_no" => 0,
						"gh_function_desc" => $fc,
						"gh_min_seq_no" => 0,
						"gh_max_seq_no" => 999,
						"gh_last_seq_no" => 1,
						"gh_group_menu_id" => "",
						"gh_parent_menu" => "",
						"gh_menu_level" => 0,
						"gh_menu_flag" => "",
						"gh_image" => "",
						"gh_password" => "",
						"gh_form" => "",
						"gh_menu_seq" => 0,
						"gh_old_app" => "",
						"gh_function_code" => $fc,
					);
					$dataInsert = gsGenHardcodedModel::create($dataInsertes);					
					$notrans = "W".$fn."001";
		}
		
		if($notrans == null){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return response()->json(['notrans' => $notrans]);
        }
    }
	
}
