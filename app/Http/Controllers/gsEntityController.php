<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\gsEntityModel;
use Illuminate\Support\Facades\DB;

class gsEntityController extends Controller
{
    public function index()
    {
        $data = gsEntityModel::all();
        $data_count = count($data);
        
		if(!$data){
          return response('there is something wrong', 500)->header('Content-Type', 'text/plain');
        }else{
          return $data;
        }
    }
}
