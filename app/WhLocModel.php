<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhLocModel extends Model
{
    protected $table = 'IM_WH_LOC';
    protected $primary_key = '';
    protected $fillable = [''];
    public $timestamps = false;
}
