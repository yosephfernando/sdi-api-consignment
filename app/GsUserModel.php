<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GsUserModel extends Model
{
    protected $table = 'GS_USERS_SECURITY';
    protected $primary_key = 'gu_user_id';
    protected $fillable = [''];
    public $timestamps = false;
}
