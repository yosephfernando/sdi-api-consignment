<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsignmentModel extends Model
{
    protected $table = 'Name';
    protected $primary_key = 'id';
    protected $fillable = ['firstame', 'lastname'];
    public $timestamps = false;
}
