<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class consigmentModel extends Model
{
    protected $table = 'Name';
    protected $filable = ['id', 'firstname', 'lastname'];
    protected $primaryKey = 'id';
}
