<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClosingDateModel extends Model
{
    protected $table = 'CLOSING_DATE';
    protected $primary_key = 'MODULE_NAME';
    protected $fillable = [''];
    public $timestamps = false;
}
