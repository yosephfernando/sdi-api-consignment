<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gsEntityModel extends Model
{
  protected $table = 'GS_ENTITY';
  //protected $primaryKey = '';
  //protected $guarded  = ['id_pk'];
  public $timestamps = false;
}
