<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferDetailModel extends Model
{
    protected $table = 'IM_TRANSFER_DETAIL';
    protected $primary_key = 'trfd_seqno';
    protected $fillable = ['trfd_seqno', 'trfd_fr1', 'trfd_fr2', 'trfd_prd_code', 'trfd_prd_grade', 'trfd_prd_size', 'trfd_qty', 'trfd_price', 'trfd_cogs', 'trfd_flag', 'trfd_note'];
    public $timestamps = false;
}
