<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prdPriceModel extends Model
{
  protected $table = 'IM_PRD_PRICE';
  //protected $primaryKey = '';
  //protected $guarded  = ['id_pk'];
  public $timestamps = false;
}
