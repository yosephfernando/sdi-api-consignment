<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prdMasterModel extends Model
{
  protected $table = 'IM_PRD_MASTER';
  //protected $primaryKey = '';
  //protected $guarded  = ['id_pk'];
  public $timestamps = false;
}
