<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	Route::get('/tes', function(){
			phpinfo();
	});
Route::group(['prefix'=>'api', 'middleware' => 'auth.basic'], function(){
	Route::get('/consigement', 'ConsignmentController@index');
    Route::get('/consigement/{sh_code}', 'ConsignmentController@showDataById');
	Route::get('/consigement/{sh_code}/{date}', 'ConsignmentController@listConsignment');
    Route::get('/events/{username}/{date}', 'events@index');
	Route::get('/events/{username}/{date}/{gpAcara}', 'events@eventDistync');
    Route::get('/prd-line/{userId}', 'prdLineController@index');
    Route::post('/prd-price-filter', 'prdPriceController@getByFilter');
    Route::post('/ob-price-filter', 'obPriceListController@getByFilter');
    Route::post('/im-cost-std-filter', 'imCostStdController@getByFilter');
    Route::get('/users/{userId}', 'UsersController@index');
    Route::get('/spg/{sh_code}', 'spgController@index');
    Route::get('/pos-showroom/{psr_showroom1_id}', 'posShowroomMaster@index');
    Route::post('/web-sales-detail', 'webSalesDetail@insertSalesDetail');
    Route::post('/web-sales-header', 'webSalesHeader@insertSalesHeader');
	Route::post('/delete-consignment', 'webSalesHeader@deleteCons');
	Route::get('/web-sales-header-index', 'webSalesHeader@index');
    Route::get('/produk-master-filter/{article}', 'prdMaster@index');
	Route::get('/produk-master-filter/{article}/{size}', 'prdMaster@getByartAndSize');
	Route::get('/produk-master-filter/{article}/{date}/{step}', 'prdMaster@getByartc');
	Route::get('/produk-master-sugest/{keyword}', 'prdMaster@getSugest');
	Route::get('/gs-entity', 'gsEntityController@index');
	Route::get('/gs-gen/{fc}/{fn}/{showroom}', 'gsGenHarcodedController@getGsGenByFcFn');
});
